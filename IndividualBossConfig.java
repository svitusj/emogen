
import java.io.IOException;
import java.util.Random;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class IndividualBossConfig extends Individual
{

  /////////////////////////////////////////////////////////////////////////////

  public enum VersionType
  {
    VERSION_TYPE_1, VERSION_TYPE_2,
  }

  public enum AddOnType
  {
    ADDON_TYPE_VITAL_DEFAULT, ADDON_TYPE_WEAPON_MELEE, ADDON_TYPE_WEAPON_BULLET, ADDON_TYPE_WEAPON_HOMING, ADDON_TYPE_WEAPON_LASER,
    //
    ADDON_TYPE_HULL_ENABLED, ADDON_TYPE_LINK_PARENT, ADDON_TYPE_MOVEMENT_LEAD
  }

  public enum DuelResultType
  {
    DUEL_RESULT_TYPE_DRAW, DUEL_RESULT_TYPE_PLAYER_WINS, DUEL_RESULT_TYPE_BOSS_WINS
  }

  static class Quality
  {
    public Quality()
    {
    }

    double L_Valid       = 0;
    double Q_Completion  = 0;
    double Q_Duration    = 0;
    double Q_Uncertainty = 0;
    double Q_KillerMoves = 0;
    double Q_Permanence  = 0;
    double Q_LeadChange  = 0;
    double Q_Overall     = 0;
  };

  /////////////////////////////////////////////////////////////////////////////
  
  static private final boolean     IS_REPARATION_ENABLED = false;

  static private final VersionType VERSION_TYPE          = VersionType.VERSION_TYPE_2;

  static private final char        INVALID_CHARACTER     = '-';

  // private static final int mNumberOfWeaponTypes =
  // GetNumberOfRowsInEncoding();

  private ArrayList<Integer>       mHullsEnabledIndices;                                  // Number
                                                                                          // of
                                                                                          // enabled
                                                                                          // hulls

  private String                   mConfiguration;                                        // Configuration
                                                                                          // value

  private String                   mDescription;                                          // Description

  private boolean                  mIsFitnessCalculated = false;                          // False
                                                                                          // until
                                                                                          // fitness
                                                                                          // is
                                                                                          // calculated

  private Quality                  mQuality;                                              // Quality

  static private Random            random               = new Random( System.nanoTime() );

  /////////////////////////////////////////////////////////////////////////////

  static public int GetNumberOfRowsInEncoding()
  {
    int numberOfRowsInEncoding = 0;
    switch ( VERSION_TYPE ) {
      case VERSION_TYPE_1:
        numberOfRowsInEncoding = 5;
        break;
      case VERSION_TYPE_2:
        numberOfRowsInEncoding = 8;
        break;
      default:
        System.out.println( "Version type out of range." );
        break;
    }
    return numberOfRowsInEncoding;
  }

  /////////////////////////////////////////////////////////////////////////////

  static public char GetCharacterFromNumber( int number )
  {
    char text = INVALID_CHARACTER;

    switch ( number ) {
      case -1:
        text = INVALID_CHARACTER;
        break;

      case 0:
        text = '0';
        break;
      case 1:
        text = '1';
        break;
      case 2:
        text = '2';
        break;
      case 3:
        text = '3';
        break;
      case 4:
        text = '4';
        break;
      case 5:
        text = '5';
        break;
      case 6:
        text = '6';
        break;
      case 7:
        text = '7';
        break;
      case 8:
        text = '8';
        break;
      case 9:
        text = '9';
        break;

      case 10:
        text = 'a';
        break;
      case 11:
        text = 'b';
        break;
      case 12:
        text = 'c';
        break;
      case 13:
        text = 'd';
        break;
      case 14:
        text = 'e';
        break;
      case 15:
        text = 'f';
        break;
      case 16:
        text = 'g';
        break;
      case 17:
        text = 'h';
        break;
      case 18:
        text = 'i';
        break;
      case 19:
        text = 'j';
        break;

      case 20:
        text = 'k';
        break;
      case 21:
        text = 'l';
        break;
      case 22:
        text = 'm';
        break;
      case 23:
        text = 'n';
        break;
      case 24:
        text = 'o';
        break;
      case 25:
        text = 'p';
        break;
      case 26:
        text = 'q';
        break;
      case 27:
        text = 'r';
        break;
      case 28:
        text = 's';
        break;
      case 29:
        text = 't';
        break;

      case 30:
        text = 'u';
        break;
      case 31:
        text = 'v';
        break;
      case 32:
        text = 'w';
        break;
      case 33:
        text = 'x';
        break;
      case 34:
        text = 'y';
        break;
      case 35:
        text = 'z';
        break;
      case 36:
        text = 'A';
        break;
      case 37:
        text = 'B';
        break;
      case 38:
        text = 'C';
        break;
      case 39:
        text = 'D';
        break;

      case 40:
        text = 'E';
        break;
      case 41:
        text = 'F';
        break;
      case 42:
        text = 'G';
        break;
      case 43:
        text = 'H';
        break;
      case 44:
        text = 'I';
        break;
      case 45:
        text = 'J';
        break;
      case 46:
        text = 'K';
        break;
      case 47:
        text = 'L';
        break;
      case 48:
        text = 'M';
        break;
      case 49:
        text = 'N';
        break;

      case 50:
        text = 'O';
        break;
      case 51:
        text = 'P';
        break;
      case 52:
        text = 'Q';
        break;
      case 53:
        text = 'R';
        break;
      case 54:
        text = 'S';
        break;
      case 55:
        text = 'T';
        break;
      case 56:
        text = 'U';
        break;
      case 57:
        text = 'V';
        break;
      case 58:
        text = 'W';
        break;
      case 59:
        text = 'X';
        break;

      case 60:
        text = 'Y';
        break;
      case 61:
        text = 'Z';
        break;
      case 62:
        text = '[';
        break;
      case 63:
        text = ']';
        break;

      default:
        System.out.println( "Number out of range" );
        break;
    }

    return text;
  }

  /////////////////////////////////////////////////////////////////////////////

  public IndividualBossConfig( String configuration )
  {
    mDescription = "";
    mQuality = new Quality();
    mConfiguration = configuration;
    mHullsEnabledIndices = new ArrayList<Integer>();

    int hullSize = mConfiguration.length() / GetNumberOfRowsInEncoding();
    for ( int i = 0; i < mConfiguration.length(); ++i ) {
      if ( i >= ( 5 * hullSize ) && i < ( 6 * hullSize ) ) {
        // Enabled
        if ( mConfiguration.charAt( i ) == '1' ) {
          mHullsEnabledIndices.add( i % hullSize );
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  public IndividualBossConfig( int hullSize, double oneProbability )
  {
    mDescription = "";
    mQuality = new Quality();
    // Random random = new Random( System.nanoTime() );
    mConfiguration = "";
    mHullsEnabledIndices = new ArrayList<Integer>();
    int size = hullSize * GetNumberOfRowsInEncoding();
    ArrayList<Integer> hullsEnabled = new ArrayList<Integer>();
    hullsEnabled.add( -1 );

    for ( int i = 0; i < size; ++i ) {
      if ( i >= 7 * hullSize ) {
        // Movement lead
        mConfiguration += random.nextDouble() <= oneProbability ? "1" : "0";
      }
      else if ( i >= 6 * hullSize ) {
        // Link parent
        if ( IS_REPARATION_ENABLED ) {
          mConfiguration += ( mConfiguration.charAt( i - hullSize ) == '0' ) ? INVALID_CHARACTER
              : GetCharacterFromNumber( hullsEnabled.get( random.nextInt( hullsEnabled.size() ) ) );
        }
        else {
          int linkParentHullIndex = random.nextInt( hullSize ) - 1;
          mConfiguration += GetCharacterFromNumber( linkParentHullIndex );
          if ( IsValid() && !mHullsEnabledIndices.contains( linkParentHullIndex ) ) {
            SetAsInvalid();
          }
        }
      }
      else if ( i >= 5 * hullSize ) {
        // Enabled
        if ( random.nextDouble() <= oneProbability ) {
          mConfiguration += "1";
          mHullsEnabledIndices.add( i % hullSize );
          hullsEnabled.add( i % hullSize );
        }
        else {
          mConfiguration += "0";
        }
      }
      else {
        mConfiguration += random.nextDouble() <= oneProbability ? "1" : "0";
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  public String GetConfiguration()
  {
    return mConfiguration;
  }

  /////////////////////////////////////////////////////////////////////////////

  public void SetConfiguration( String configuration )
  {
    mConfiguration = configuration;
  }

  /////////////////////////////////////////////////////////////////////////////

  public Quality GetQuality()
  {
    return mQuality;
  }

  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////

  public String GetContentAsString()
  {
    return mConfiguration;
  }

  /////////////////////////////////////////////////////////////////////////////

  public String GetDescriptionString()
  {
    return mDescription;
  }

  /////////////////////////////////////////////////////////////////////////////

  public Individual CrossOver( Individual mate )
  {
    String newConfiguration = "";
    String mateConfiguration = ( ( IndividualBossConfig ) mate ).GetConfiguration();
    int minimumLength = Math.min( mConfiguration.length(), mateConfiguration.length() );
    int maximumLength = Math.max( mConfiguration.length(), mateConfiguration.length() );
    int crossCutIndex = random.nextInt( minimumLength );

    for ( int i = 0; i < minimumLength; ++i ) {
      newConfiguration += ( i <= crossCutIndex ) ? mConfiguration.charAt( i ) : mateConfiguration.charAt( i );
    }
    for ( int j = 0; j < ( maximumLength - minimumLength ); ++j ) {
      newConfiguration += '0';
    }
    // System.out.println( ">>>crossCutIndex: " + crossCutIndex );
    // System.out.println( ">>>Configuration1: " + mConfiguration );
    // System.out.println( ">>>Configuration2: " + mateConfiguration );
    // System.out.println( ">>>New: " + newConfiguration );
    return new IndividualBossConfig( newConfiguration );
  }

  /////////////////////////////////////////////////////////////////////////////

  public void Mutate()
  {
    // Random random = new Random( System.nanoTime() );
    StringBuilder newConfiguration = new StringBuilder( mConfiguration );
    int hullSize = newConfiguration.length() / GetNumberOfRowsInEncoding();
    for ( int i = 0; i < newConfiguration.length(); ++i ) {
      if ( random.nextInt( newConfiguration.length() ) == 0 ) {

        if ( i >= ( 6 * hullSize ) && i < ( 7 * hullSize ) ) {
          // Link parent
          if ( random.nextInt( mHullsEnabledIndices.size() + 1 ) == 0 ) {
            newConfiguration.setCharAt( i, INVALID_CHARACTER );
          }
          else {
            newConfiguration.setCharAt( i,
                GetCharacterFromNumber( mHullsEnabledIndices.get( random.nextInt( mHullsEnabledIndices.size() ) ) ) );
          }
        }
        else if ( i >= ( 5 * hullSize ) && i < ( 6 * hullSize ) ) {
          // Enabled
          newConfiguration.setCharAt( i, ( newConfiguration.charAt( i ) == '0' ) ? '1' : '0' );
          if ( newConfiguration.charAt( i ) == '1' ) {
            mHullsEnabledIndices.add( i % hullSize );
          }
          else {
            boolean b = mHullsEnabledIndices.remove( Integer.valueOf( i % hullSize ) );
          }
        }
        else {
          newConfiguration.setCharAt( i, ( newConfiguration.charAt( i ) == '0' ) ? '1' : '0' );
        }
      }
    }
    mConfiguration = newConfiguration.toString();
  }

  /////////////////////////////////////////////////////////////////////////////

  public double CalculateFitnessValue()
  {
    if ( mIsFitnessCalculated ) {
      return GetFitnessValue();
    }
    
    // Size evaluation
    if ( ( mConfiguration.length() % GetNumberOfRowsInEncoding() ) != 0 ) {
      System.out.println( "Configuration Error. Size: " + mConfiguration.length() );
      try {
        System.in.read();
      }
      catch ( IOException e ) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    

    
    // Hull size
    int numberOfHulls = mConfiguration.length() /  GetNumberOfRowsInEncoding();
    // Hull structure
    int numberOfVitals = 0;
    int numberOfMelees = 0;
    int numberOfBullets = 0;
    int numberOfHomings = 0;
    int numberOfLasers = 0;
    ArrayList< HashSet< AddOnType > > hulls = new ArrayList< HashSet< AddOnType > >();
    boolean isValid = true;
    for ( int g = 0; isValid && g < numberOfHulls; ++g ) {
      HashSet< AddOnType > addOns = new HashSet< AddOnType >();
      if ( mHullsEnabledIndices.isEmpty() || mHullsEnabledIndices.contains( g ) ) {
        boolean isWeaponFree = true;
        if ( mConfiguration.charAt( g + numberOfHulls * 0 ) == '1' ) { addOns.add( AddOnType.ADDON_TYPE_VITAL_DEFAULT ); ++numberOfVitals; }
        if ( mConfiguration.charAt( g + numberOfHulls * 1 ) == '1' ) { addOns.add( AddOnType.ADDON_TYPE_WEAPON_MELEE );  ++numberOfMelees; isWeaponFree = false; }
        if ( mConfiguration.charAt( g + numberOfHulls * 2 ) == '1' ) { addOns.add( AddOnType.ADDON_TYPE_WEAPON_BULLET ); ++numberOfBullets; isWeaponFree = false; }
        if ( mConfiguration.charAt( g + numberOfHulls * 3 ) == '1' ) { addOns.add( AddOnType.ADDON_TYPE_WEAPON_HOMING ); ++numberOfHomings; isWeaponFree = false; }
        if ( mConfiguration.charAt( g + numberOfHulls * 4 ) == '1' ) { addOns.add( AddOnType.ADDON_TYPE_WEAPON_LASER );  ++numberOfLasers; isWeaponFree = false; }
        if ( isWeaponFree && GetNumberOfRowsInEncoding() > 5 && mConfiguration.charAt( g + numberOfHulls * 7 ) == '1' ) {
          // Motion lead hull found with no weapons
          isValid = false;
        }
      }
      hulls.add( addOns );
    }
    if ( !isValid ) {
      SetAsInvalid();
    }
    
    
    

    
  
  
  
  
  
  
  
  
  
  
  
  
    // Distance from last vital hull to end
    int distanceFromEndToLastVitalHullIndex = -1;
    int numberOfDisabledHulls = 0;
    //System.out.println( ">>>mHullEnabledindices " + mHullsEnabledIndices.toString() );
    for ( int h = hulls.size() - 1; distanceFromEndToLastVitalHullIndex < 0 && h >= 0; --h ) {
      if ( mHullsEnabledIndices.isEmpty() || mHullsEnabledIndices.contains( h ) ) {
        if ( hulls.get( h ).contains( AddOnType.ADDON_TYPE_VITAL_DEFAULT ) ) {
          distanceFromEndToLastVitalHullIndex = hulls.size() - 1 - h - numberOfDisabledHulls;
        }
      }
      else {
        ++numberOfDisabledHulls;
      }
    }
    //System.out.println( ">>numberOfDisabledHulls " + numberOfDisabledHulls );
    //System.out.println( ">>distanceFromEndToLastVitalHullIndex " + distanceFromEndToLastVitalHullIndex );
   

    // Duel settings
    //Random random                        = new Random( System.nanoTime() );
    final int    numberOfDuels           = 30;
    final int    optimalDuelSteps        = 60 * ( 10 + 5  * ( numberOfHulls - 32 ) / 32 );
    final int    maximumDuelSteps        = 60 * ( 20 + 10 * ( numberOfHulls - 32 ) / 32 );
    final int    hullVisitSteps          = 20;
    final int    bossInitialShields      = numberOfVitals + 1;
    final int    playerInitialShields    = 5;
    final int    recoveryStepDistance    = 10;
    final float  killerLifePercentageGap = 0.3f;
    final double hitProbabilityMelee     = 0.014;//0.014;
    final double hitProbabilityBullet    = 0.014;//0.014;
    final double hitProbabilityHoming    = 0.014;//0.014;
    final double hitProbabilityLaser     = 0.014;//0.014;
    final double hitProbabilityPlayer    = 0.25;//0.2;
    
    // Duel stat variables
    int numberOfDuelsWonBySomeone = 0;
    int numberOfPlayerWins        = 0;
    
    // Duel stats
    double L_Valid       = 0.0;
    double Q_Completion  = 0.0;
    double Q_Duration    = 0.0;
    double Q_Uncertainty = 0.0;
    double Q_KillerMoves = 0.0;
    double Q_Permanence  = 0.0;
    double Q_LeadChange  = 0.0;
    double Q_Overall     = 0.0;
    
    double F_PlayerWinPercentage        = 0.0;
    double F_PlayerWinLifePercentage    = 0.0;
    double F_PlayerWinLifeGapPercentage = 0.0;
    double F_Overall                    = 0.0;
    
    double X_PlayerLifePercentage       = 0.0;
    double X_BossLifePercentage         = 0.0;
    
    // Fitness references
    final double goalPlayerWinPercentage        = 0.33;
    final double goalPlayerWinLifePercentage    = 0.35;
    final double goalPlayerWinLifeGapPercentage = 0.35;
    
    
    for ( int d = 0; d < numberOfDuels; ++d ) {
      
      // Settings before each duel
      DuelResultType     duelResult             = DuelResultType.DUEL_RESULT_TYPE_DRAW;
      DuelResultType     provisionalLead        = DuelResultType.DUEL_RESULT_TYPE_DRAW;
      HashSet< Integer > vitalPointsDestroyed   = new HashSet< Integer >();
      boolean            isForwardRoute         = true;
      boolean            isDuelInProgress       = true;
      int                bossShields            = bossInitialShields;
      int                playerShields          = playerInitialShields;
      int                hullStepCount          = 0;
      int                duelStepCount          = 0;
      int                bossStepsToCritical    = 0;
      int                playerStepsToCritical  = 0;
      int                highlightMoveCount     = 0;
      int                killerMoveCount        = 0;
      int                recoveryMoveCount      = 0;
      int                lastHighlightStepCount = 0;
      int                leadChangeCount        = 0;
      float              bossLifePercentage     = 1.0f;
      float              playerLifePercentage   = 1.0f;
      
      
      // Duel loop
      for ( int hullIndex = 0; isDuelInProgress && duelStepCount < maximumDuelSteps; ++duelStepCount ) {
        
        boolean isHullStudied = true;
        if ( !mHullsEnabledIndices.isEmpty() && !mHullsEnabledIndices.contains( hullIndex ) ) {
          --duelStepCount;
          isHullStudied = false;
        }
        
        
        // Variables used in player attack, later
        boolean isVitalHullAlreadyDestroyed = false;
        boolean isBossDamaged = false;
        
        
        if ( isHullStudied ) {
           
          // Boss attack
          
          // Index range
          ArrayList< Integer > indicesInRange = new ArrayList< Integer >();
          int indexInRange = hullIndex - 1;
          int indicesAdded = 0;
          while ( indicesAdded < 3 && indexInRange >= 0 ) {
            if ( mHullsEnabledIndices.isEmpty() || mHullsEnabledIndices.contains( indexInRange ) ) {
              indicesInRange.add( 0, indexInRange );
              ++indicesAdded;
            }
            --indexInRange;
          }
          indicesInRange.add( hullIndex );
          indexInRange = hullIndex + 1;
          indicesAdded = 0;
          while ( indicesAdded < 3 && indexInRange < hulls.size() ) {
            if ( mHullsEnabledIndices.isEmpty() || mHullsEnabledIndices.contains( indexInRange ) ) {
              indicesInRange.add( indexInRange );
              ++indicesAdded;
            }
            ++indexInRange;
          }
          //int firstHullIndex = Math.max( hullIndex - 3, 0 );
          //int lastHullIndex  = Math.min( hullIndex + 3, hulls.size() - 1 );
          //System.out.println( ">>>Indices in range:" + indicesInRange.toString() );
          
          
          boolean isPlayerDamaged = false;
          //for ( int j = firstHullIndex; !isPlayerDamaged && j <= lastHullIndex; ++j ) {
          for ( int j = 0; !isPlayerDamaged && j < indicesInRange.size(); ++j ) {
            int studiedIndex = indicesInRange.get( j );
            //int distanceToCurrentHull = Math.abs( j - hullIndex );
            int distanceToCurrentHull = Math.abs( j - indicesInRange.indexOf( hullIndex ) );
            //System.out.println( ">>>Distance:" + distanceToCurrentHull );
            isPlayerDamaged = // Current hull
                ( distanceToCurrentHull < 1 && hulls.get( studiedIndex ).contains( AddOnType.ADDON_TYPE_WEAPON_MELEE )  && random.nextDouble() <= ( hitProbabilityMelee  * ( 4.0 - (double) distanceToCurrentHull ) / 4.0 ) ) ||
                ( distanceToCurrentHull < 2 && hulls.get( studiedIndex ).contains( AddOnType.ADDON_TYPE_WEAPON_BULLET ) && random.nextDouble() <= ( hitProbabilityBullet * ( 4.0 - (double) distanceToCurrentHull ) / 4.0 ) ) ||
                ( distanceToCurrentHull < 3 && hulls.get( studiedIndex ).contains( AddOnType.ADDON_TYPE_WEAPON_HOMING ) && random.nextDouble() <= ( hitProbabilityHoming * ( 4.0 - (double) distanceToCurrentHull ) / 4.0 ) ) ||
                ( distanceToCurrentHull < 4 && hulls.get( studiedIndex ).contains( AddOnType.ADDON_TYPE_WEAPON_LASER )  && random.nextDouble() <= ( hitProbabilityLaser  * ( 4.0 - (double) distanceToCurrentHull ) / 4.0 ) );
            //if ( isPlayerDamaged ) { System.out.println( ">>>Damaged" ); }
          }
          // Player health control
          if ( isDuelInProgress && isPlayerDamaged ) {
            --playerShields;
            ++highlightMoveCount;
            lastHighlightStepCount = duelStepCount;
            if ( playerShields == 1 ) {
              playerStepsToCritical = duelStepCount + 1;
            }
            else if ( playerShields <= 0 ) {
              isDuelInProgress = false;
              duelResult = DuelResultType.DUEL_RESULT_TYPE_BOSS_WINS;
            }
          }
          // Player attack
          isVitalHullAlreadyDestroyed = vitalPointsDestroyed.contains( hullIndex ) && bossShields > 1;
          isBossDamaged = hulls.get( hullIndex ).contains( AddOnType.ADDON_TYPE_VITAL_DEFAULT ) && !isVitalHullAlreadyDestroyed && random.nextDouble() <= hitProbabilityPlayer;
          // Boss health control
          if ( isDuelInProgress && isBossDamaged ) {
            vitalPointsDestroyed.add( hullIndex );
            --bossShields;
            ++highlightMoveCount;
            lastHighlightStepCount = duelStepCount;
            if ( bossShields == 1 ) {
              bossStepsToCritical = duelStepCount + 1;
            }
            if ( bossShields <= 0 ) {
              isDuelInProgress = false;
              duelResult = DuelResultType.DUEL_RESULT_TYPE_PLAYER_WINS;
            }
          }
          // Last clash evaluation
          boolean isKillerMovePossible   = Math.abs( playerLifePercentage - bossLifePercentage ) < killerLifePercentageGap;
          boolean isRecoveryMovePossible = Math.abs( lastHighlightStepCount - duelStepCount ) <= recoveryStepDistance && Math.abs( playerLifePercentage - bossLifePercentage ) >= killerLifePercentageGap;
          DuelResultType provisionalLeadOld = provisionalLead;
          playerLifePercentage = ( float ) playerShields / ( float ) playerInitialShields;
          bossLifePercentage = ( float ) bossShields / ( float ) bossInitialShields;
          provisionalLead = ( playerLifePercentage == bossLifePercentage ) ? DuelResultType.DUEL_RESULT_TYPE_DRAW : ( ( playerLifePercentage > bossLifePercentage ) ? DuelResultType.DUEL_RESULT_TYPE_PLAYER_WINS : DuelResultType.DUEL_RESULT_TYPE_BOSS_WINS );
          if ( provisionalLead != provisionalLeadOld ) {
            ++leadChangeCount;
          }
          if ( isKillerMovePossible && Math.abs( playerLifePercentage - bossLifePercentage ) >= killerLifePercentageGap ) {
            ++killerMoveCount;
          }
          if ( isRecoveryMovePossible && Math.abs( playerLifePercentage - bossLifePercentage ) < killerLifePercentageGap ) {
            ++recoveryMoveCount;
          }
        
        
        
        }
        
        
            
        
        // Route control
        ++hullStepCount;
        if ( !isHullStudied || isVitalHullAlreadyDestroyed || isBossDamaged || hullStepCount >= ( hulls.get( hullIndex ).contains( AddOnType.ADDON_TYPE_VITAL_DEFAULT ) ? ( 2 * hullVisitSteps ) : hullVisitSteps ) ) {
          hullStepCount = 0;
          // Direction control
          if ( isForwardRoute && ( hullIndex + 1 ) >= hulls.size() ) {
            if ( distanceFromEndToLastVitalHullIndex >= 10 ) {
              hullIndex = -1;
            }
            else {
              isForwardRoute = false;
            }
          }
          else if ( !isForwardRoute && ( hullIndex - 1 ) < 0 ) {
            isForwardRoute = true;
          }
          hullIndex += isForwardRoute ? 1 : -1;
          // Clamp: Example: no hulls enabled
          hullIndex = Math.max( 0, Math.min( hulls.size() - 1, hullIndex ) );
        }
      }
      //End of duel loop
      
      
      // Duel finished
      if ( duelResult != DuelResultType.DUEL_RESULT_TYPE_DRAW ) {
        ++numberOfDuelsWonBySomeone;
        if ( duelResult == DuelResultType.DUEL_RESULT_TYPE_PLAYER_WINS ) {
          ++numberOfPlayerWins;
        }
      }
      
      // Duel quality data
      //Q_Completion;
      Q_Duration    += ( double ) Math.abs( optimalDuelSteps - duelStepCount ) / ( double ) optimalDuelSteps;
      Q_Uncertainty += ( double ) ( duelStepCount - Math.min( playerStepsToCritical, bossStepsToCritical ) ) / ( double ) duelStepCount;
      Q_KillerMoves += ( double ) killerMoveCount / ( double ) highlightMoveCount;
      Q_Permanence  += ( double ) recoveryMoveCount / ( double ) ( highlightMoveCount + killerMoveCount );
      Q_LeadChange  += ( double ) leadChangeCount / ( double ) ( highlightMoveCount + killerMoveCount );
      
      // Duel fitness data
      //F_PlayerWinPercentage;
      if ( duelResult == DuelResultType.DUEL_RESULT_TYPE_PLAYER_WINS ) {
        F_PlayerWinLifePercentage    += Math.abs( goalPlayerWinLifePercentage - ( double ) playerLifePercentage ) / goalPlayerWinLifePercentage;
        F_PlayerWinLifeGapPercentage += Math.abs( goalPlayerWinLifeGapPercentage - ( double ) Math.abs( playerLifePercentage - bossLifePercentage ) ) / goalPlayerWinLifeGapPercentage;
        X_PlayerLifePercentage       += ( double ) playerLifePercentage;
        X_BossLifePercentage         += ( double ) bossLifePercentage;
        //System.out.println( "----Duel Data----" );
        //System.out.println( "F_PlayerWinLifePercentage " + Math.abs( goalPlayerWinLifePercentage - ( double ) playerLifePercentage ) / goalPlayerWinLifePercentage );
        //System.out.println( "playerLifePercentage " + playerLifePercentage );
        //System.out.println( "goalPlayerWinLifePercentage " + goalPlayerWinLifePercentage );
        //System.out.println( "goalPlayerWinLifePercentage - ( double ) playerLifePercentage " + (goalPlayerWinLifePercentage - ( double ) playerLifePercentage) );
        //System.out.println( "playerLifePercentage - bossLifePercentage: " + (playerLifePercentage - bossLifePercentage) );
        //System.exit( 0 );
      }
      
      //
//      System.out.println( "----Duel Finished----" );
//      System.out.println( "playerShields: " + playerShields );
//      System.out.println( "bossShields: " + bossShields );
//      System.out.println( "goalPlayerWinLifePercentage: " + goalPlayerWinLifePercentage );
//      System.out.println( "playerLifePercentage: " + playerLifePercentage );
//      System.out.println( "F_PlayerWinLifePercentage: " + F_PlayerWinLifePercentage );
//      System.exit( 0 );
      
    }
    // Every duel finished
  
 
  
  
    // Global duel quality stats
    int qualityStatCount = 0;
    Q_Completion         = ( double ) numberOfDuelsWonBySomeone / ( double ) numberOfDuels; ++qualityStatCount;
    Q_Duration           = Math.max( 0.0, 1.0 - Q_Duration / ( double ) numberOfDuels );                     ++qualityStatCount;
    Q_Uncertainty        = Math.max( 0.0, 1.0 - Q_Uncertainty / ( double ) numberOfDuels );                  ++qualityStatCount;
    Q_KillerMoves        = Math.max( 0.0, 1.0 - Q_KillerMoves / ( double ) numberOfDuels );                  ++qualityStatCount;
    Q_Permanence         = Math.max( 0.0, 1.0 - Q_Permanence / ( double ) numberOfDuels );                   ++qualityStatCount;
    Q_LeadChange         = Q_LeadChange / ( double ) numberOfDuels;                         ++qualityStatCount;
    Q_Overall            = ( Q_Completion + Q_Duration + Q_Uncertainty + Q_KillerMoves + Q_Permanence + Q_LeadChange ) / ( double ) qualityStatCount;
    //
//    System.out.println( "----Quality----" );
//    System.out.println( "Q_Completion: " + Q_Completion );
//    System.out.println( "Q_Duration: " + Q_Duration );
//    System.out.println( "Q_Uncertainty: " + Q_Uncertainty );
//    System.out.println( "Q_KillerMoves: " + Q_KillerMoves );
//    System.out.println( "Q_Permanence: " + Q_Permanence );
//    System.out.println( "Q_LeadChange: " + Q_LeadChange );
//    System.out.println( "Q_Overall: " + Q_Overall );
    
    // Fitness stats
    int fitnessStatCount         = 0;
    F_PlayerWinPercentage        = Math.max( 0.0, 1.0 - ( ( double ) Math.abs( goalPlayerWinPercentage - ( ( double ) numberOfPlayerWins / ( double ) numberOfDuels ) ) ) / goalPlayerWinPercentage ); ++fitnessStatCount;
    F_PlayerWinLifePercentage    = ( numberOfPlayerWins > 0 ) ? Math.max( 0.0, 1.0 - F_PlayerWinLifePercentage / ( double ) numberOfPlayerWins ) : 0.0;                                            ++fitnessStatCount;
    F_PlayerWinLifeGapPercentage = ( numberOfPlayerWins > 0 ) ? Math.max( 0.0, 1.0 - F_PlayerWinLifeGapPercentage / ( double ) numberOfPlayerWins ) : 0.0;                                         ++fitnessStatCount;
    F_Overall                    = ( F_PlayerWinPercentage + F_PlayerWinLifePercentage + F_PlayerWinLifeGapPercentage ) / ( double ) fitnessStatCount;
    X_PlayerLifePercentage       = ( numberOfPlayerWins > 0 ) ? ( X_PlayerLifePercentage / ( double ) numberOfPlayerWins ) : 0.0;
    X_BossLifePercentage         = ( numberOfPlayerWins > 0 ) ? ( X_BossLifePercentage / ( double ) numberOfPlayerWins ) : 0.0; 

    
    //
//    System.out.println( "----Fitness----" );
//    System.out.println( "numberOfDuels: " + numberOfDuels );
//    System.out.println( "numberOfPlayerWins: " + numberOfPlayerWins );
//    System.out.println( "F_PlayerWinPercentage: " + F_PlayerWinPercentage );
//    System.out.println( "F_PlayerWinLifePercentage: " + F_PlayerWinLifePercentage );
//    System.out.println( "F_PlayerWinLifeGapPercentage: " + F_PlayerWinLifeGapPercentage );
//    System.out.println( "F_Overall: " + F_Overall );
//    System.out.println( "X_PlayerLifePercentage: " + X_PlayerLifePercentage );
//    System.out.println( "X_BossLifePercentage: " + X_BossLifePercentage );
    
    mDescription =  "";
    mDescription += "----Content----\n";
    mDescription += GetContentAsString() + "\n";
    mDescription += "----Fitness----\n";
    mDescription += "numberOfDuels: " + numberOfDuels + "\n";
    mDescription += "numberOfPlayerWins: " + numberOfPlayerWins + "\n";
    mDescription += "F_PlayerWinPercentage: " + F_PlayerWinPercentage + "\n";
    mDescription += "F_PlayerWinLifePercentage: " + F_PlayerWinLifePercentage + "\n";
    mDescription += "F_PlayerWinLifeGapPercentage: " + F_PlayerWinLifeGapPercentage + "\n";
    mDescription += "F_Overall: " + F_Overall + "\n";
    mDescription += "----Quality----\n";
    mDescription += "Q_Completion: " + Q_Completion + "\n";
    mDescription += "Q_Duration: " + Q_Duration + "\n";
    mDescription += "Q_Uncertainty: " + Q_Uncertainty + "\n";
    mDescription += "Q_KillerMoves: " + Q_KillerMoves + "\n";
    mDescription += "Q_Permanence: " + Q_Permanence + "\n";
    mDescription += "Q_LeadChange: " + Q_LeadChange + "\n";
    mDescription += "Q_Overall: " + Q_Overall + "\n";
    
    //
    //
    //System.out.println( mDescription );
    //System.exit( 0 );
    //
    //
    
    mQuality.Q_Completion  = IsValid() ? Q_Completion : 0.0;
    mQuality.Q_Duration    = IsValid() ? Q_Duration : 0.0;
    mQuality.Q_Uncertainty = IsValid() ? Q_Uncertainty : 0.0;
    mQuality.Q_KillerMoves = IsValid() ? Q_KillerMoves : 0.0;
    mQuality.Q_Permanence  = IsValid() ? Q_Permanence : 0.0;
    mQuality.Q_LeadChange  = IsValid() ? Q_LeadChange : 0.0;
    mQuality.Q_Overall     = IsValid() ? Q_Overall : 0.0;
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
    mIsFitnessCalculated = true;
    return IsValid() ? F_Overall : 0.0;
    
    


    

  }

  /////////////////////////////////////////////////////////////////////////////
}
