# README - EMoGen GENERAL INFORMATION #

* **Project ** EMoGen - Evolutionary Model Generation
* **SVIT Research Group** https://svit.usj.es/
* **Universidad San Jorge** https://www.usj.es/

### CONTENT ###

* **./**  -  EMoGen prototype source code
	 
* **./ModelExamples**  -  SDML (Shooter Definition Model Language) model examples
    
### INSTRUCTIONS ###

* Main file: EMoGen.java
* Uncomment one of the examples available, e.g., tinyEvo3: The original bosses in Kromaia, a commercial video game scase study, are used as the starting population
